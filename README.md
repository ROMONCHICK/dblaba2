# Лабараторная работа 2

### Создание таблиц

```sql
CREATE TABLE IF NOT EXISTS "videos" (
	"video_id" integer NOT NULL,
	"video_number" bigint NOT NULL,
	"video_type" text NOT NULL,
	"upload_date" date NOT NULL,
	"supplier_company" text NOT NULL,
	"total_duration" double precision NOT NULL,
	"category" text NOT NULL,
	"viewing_price" double precision NOT NULL,
	PRIMARY KEY ("video_id")
);

CREATE TABLE IF NOT EXISTS "users" (
	"user_id" integer NOT NULL,
	"full_name" text NOT NULL,
	"username" text NOT NULL UNIQUE,
	"address" text NOT NULL,
	"phone" text NOT NULL,
	PRIMARY KEY ("user_id")
);

CREATE TABLE IF NOT EXISTS "userdownloads" (
	"download_id" integer NOT NULL,
	"user_id" integer NOT NULL UNIQUE,
	"video_id" integer NOT NULL UNIQUE,
	"download_date" date NOT NULL,
	"price_in_event" double precision NOT NULL,
	"License_id" bigint NOT NULL,
	PRIMARY KEY ("download_id")
);

CREATE TABLE IF NOT EXISTS "videohost" (
	"user" bigint NOT NULL,
	"Video_ID_number" bigint NOT NULL,
	"download_date" timestamp with time zone NOT NULL,
	"price" bigint NOT NULL,
	"date_of_purchase" bigint NOT NULL,
	"video_number" bigint NOT NULL,
	"upload_date" date NOT NULL,
	"supplier_company" bigint NOT NULL,
	PRIMARY KEY ("user", "Video_ID_number", "download_date")
);

CREATE TABLE IF NOT EXISTS "License" (
	"id" bigint GENERATED ALWAYS AS IDENTITY NOT NULL UNIQUE,
	"date_of_issue" timestamp with time zone NOT NULL,
	"to_user" bigint NOT NULL,
	"to_video" bigint NOT NULL,
	PRIMARY KEY ("id")
);
```

### Создание ограничений

```sql
ALTER TABLE "userdownloads" ADD CONSTRAINT "userdownloads_fk1" FOREIGN KEY ("user_id") REFERENCES "users"("user_id");

ALTER TABLE "userdownloads" ADD CONSTRAINT "userdownloads_fk2" FOREIGN KEY ("video_id") REFERENCES "videos"("video_id");

ALTER TABLE "userdownloads" ADD CONSTRAINT "userdownloads_fk5" FOREIGN KEY ("License_id") REFERENCES "License"("id");

ALTER TABLE "License" ADD CONSTRAINT "License_fk3" FOREIGN KEY ("to_video") REFERENCES "videos"("video_id");
```

### Вставка данных

```sql
-- Вставка данных в таблицу videos
INSERT INTO "videos" (video_id, video_number, video_type, upload_date, supplier_company, total_duration, category, viewing_price)
VALUES
(1, 1001, 'Documentary', '2023-01-15', 'Nature Films Ltd.', 90.5, 'Nature', 15.00),
(2, 1002, 'Drama', '2023-02-20', 'Drama Productions', 120.0, 'Entertainment', 20.00),
(3, 1003, 'Comedy', '2023-03-18', 'Laughs Inc.', 110.3, 'Entertainment', 18.00),
(4, 1004, 'Horror', '2023-04-22', 'Scary Movies Co.', 95.7, 'Horror', 22.00),
(5, 1005, 'Action', '2023-05-25', 'Action Studios', 130.0, 'Action', 25.00);

-- Вставка данных в таблицу users
INSERT INTO "users" (user_id, full_name, username, address, phone)
VALUES
(1, 'Иван Иванов', 'ivanov', 'ул. Пушкина, д. 10', '123-456-7890'),
(2, 'Мария Петрова', 'petrova', 'ул. Ленина, д. 20', '098-765-4321'),
(3, 'Сергей Сидоров', 'sidorov', 'ул. Гагарина, д. 30', '567-890-1234'),
(4, 'Елена Кузнецова', 'kuznetsova', 'ул. Тверская, д. 40', '432-109-8765'),
(5, 'Алексей Смирнов', 'smirnov', 'ул. Новая, д. 50', '321-654-0987');

-- Вставка данных в таблицу License
INSERT INTO "License" (date_of_issue, to_user, to_video)
VALUES
('2023-01-16 10:00:00+00', 1, 1),
('2023-02-21 11:00:00+00', 2, 2),
('2023-03-19 12:00:00+00', 3, 3),
('2023-04-23 13:00:00+00', 4, 4),
('2023-05-26 14:00:00+00', 5, 5);

-- Вставка данных в таблицу userdownloads
INSERT INTO "userdownloads" (download_id, user_id, video_id, download_date, price_in_event, License_id)
VALUES
(1, 1, 1, '2023-01-16', 15.00, 1),
(2, 2, 2, '2023-02-21', 20.00, 2),
(3, 3, 3, '2023-03-19', 18.00, 3),
(4, 4, 4, '2023-04-23', 22.00, 4),
(5, 5, 5, '2023-05-26', 25.00, 5);

-- Вставка данных в таблицу videohost
INSERT INTO "videohost" (user, Video_ID_number, download_date, price, date_of_purchase, video_number, upload_date, supplier_company)
VALUES
(1, 1001, '2023-01-16 10:00:00+00', 15, 1673871600, 1001, '2023-01-15', 1001),
(2, 1002, '2023-02-21 11:00:00+00', 20, 1676871600, 1002, '2023-02-20', 1002),
(3, 1003, '2023-03-19 12:00:00+00', 18, 1679871600, 1003, '2023-03-18', 1003),
(4, 1004, '2023-04-23 13:00:00+00', 22, 1682871600, 1004, '2023-04-22', 1004),
(5, 1005, '2023-05-26 14:00:00+00', 25, 1685871600, 1005, '2023-05-25', 1005);
```

Эти запросы создадут таблицы, добавят необходимые ограничения и заполнят таблицы данными.

### Простые запросы

1. **Получить все записи из таблицы `videos`**
```sql
SELECT * FROM "videos";
```

2. **Получить название и тип всех видео**
```sql
SELECT video_number, video_type FROM "videos";
```

3. **Получить имена и адреса всех пользователей**
```sql
SELECT full_name, address FROM "users";
```

### Подзапросы

#### 2.1 Подзапросы в разделе SELECT

1. **Получить имя пользователя и его последний загруженный видео ID**
```sql
SELECT u.full_name, 
       (SELECT video_id FROM "userdownloads" ud WHERE ud.user_id = u.user_id ORDER BY download_date DESC LIMIT 1) AS last_video_id
FROM "users" u;
```

#### 2.2 Подзапросы в разделе FROM

1. **Получить список видео, загруженных пользователями, и их имена**
```sql
SELECT v.video_id, v.video_number, subquery.full_name
FROM "videos" v
JOIN (SELECT u.user_id, u.full_name, ud.video_id
      FROM "users" u
      JOIN "userdownloads" ud ON u.user_id = ud.user_id) subquery
ON v.video_id = subquery.video_id;
```

#### 2.3 Подзапросы в разделе WHERE

1. **Получить все видео, которые были загружены пользователями из города "ул. Пушкина, д. 10"**
```sql
SELECT v.*
FROM "videos" v
WHERE v.video_id IN (SELECT ud.video_id
                     FROM "userdownloads" ud
                     JOIN "users" u ON ud.user_id = u.user_id
                     WHERE u.address = 'ул. Пушкина, д. 10');
```

#### 2.4 Подзапросы в разделе WITH

1. **Получить общее количество загрузок для каждого видео**
```sql
WITH video_downloads AS (
    SELECT video_id, COUNT(*) AS download_count
    FROM "userdownloads"
    GROUP BY video_id
)
SELECT v.video_id, v.video_number, vd.download_count
FROM "videos" v
LEFT JOIN video_downloads vd ON v.video_id = vd.video_id;
```

#### 2.5 Вывести актуальные значения (последние) по группам объектов из предметной области

1. **Получить последний загруженный видео ID и дату загрузки для каждого пользователя**
```sql
SELECT u.user_id, u.full_name, ud.video_id, ud.download_date
FROM "users" u
JOIN (SELECT user_id, video_id, download_date,
             ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY download_date DESC) AS rn
      FROM "userdownloads") ud
ON u.user_id = ud.user_id
WHERE ud.rn = 1;
```

### DML Запросы

1. **Обновить цену просмотра для всех видео в категории "Horror" на 10%**
```sql
UPDATE "videos"
SET viewing_price = viewing_price * 1.1
WHERE category = 'Horror';
```

2. **Удалить все загрузки видео для пользователя с user_id = 1**
```sql
DELETE FROM "userdownloads"
WHERE user_id = 1;
```

3. **Добавить нового пользователя**
```sql
INSERT INTO "users" (user_id, full_name, username, address, phone)
VALUES (6, 'Анна Ковалёва', 'kovaleva', 'ул. Мирная, д. 60', '987-654-3210');
```

### Примеры запросов с комментариями

1. **Получить все видео, загруженные пользователем с именем "Иван Иванов"**
```sql
SELECT v.*
FROM "videos" v
JOIN "userdownloads" ud ON v.video_id = ud.video_id
JOIN "users" u ON ud.user_id = u.user_id
WHERE u.full_name = 'Иван Иванов';
```

2. **Получить общее количество видео, загруженных каждым пользователем**
```sql
SELECT u.full_name, COUNT(ud.video_id) AS total_downloads
FROM "users" u
JOIN "userdownloads" ud ON u.user_id = ud.user_id
GROUP BY u.full_name;
```

3. **Получить все видео и информацию о пользователях, которые загрузили эти видео, включая только последние загрузки для каждого пользователя**
```sql
WITH latest_downloads AS (
    SELECT user_id, video_id, ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY download_date DESC) AS rn
    FROM "userdownloads"
)
SELECT v.video_id, v.video_number, u.full_name, ld.download_date
FROM "videos" v
JOIN latest_downloads ld ON v.video_id = ld.video_id
JOIN "users" u ON ld.user_id = u.user_id
WHERE ld.rn = 1;
```

### 3. Подзапросы

#### 3.1 Простые подзапросы

1. **Получить все видео, которые имеют цену просмотра выше средней цены просмотра всех видео**
```sql
SELECT *
FROM "videos"
WHERE viewing_price > (SELECT AVG(viewing_price) FROM "videos");
```

2. **Получить пользователей, которые загрузили видео, принадлежащее к категории "Horror"**
```sql
SELECT *
FROM "users"
WHERE user_id IN (SELECT user_id FROM "userdownloads" WHERE video_id IN (SELECT video_id FROM "videos" WHERE category = 'Horror'));
```

#### 3.2 Коррелированные подзапросы

1. **Получить список пользователей и количество видео, которые они загрузили**
```sql
SELECT u.user_id, u.full_name, 
       (SELECT COUNT(*) FROM "userdownloads" ud WHERE ud.user_id = u.user_id) AS download_count
FROM "users" u;
```

2. **Получить список видео и количество пользователей, которые их загрузили**
```sql
SELECT v.video_id, v.video_number, 
       (SELECT COUNT(*) FROM "userdownloads" ud WHERE ud.video_id = v.video_id) AS user_count
FROM "videos" v;
```

### 4. Группировка

#### 4.1 Функции: MAX, MIN, AVG, COUNT

1. **Получить максимальную, минимальную и среднюю цену просмотра всех видео**
```sql
SELECT MAX(viewing_price) AS max_price, 
       MIN(viewing_price) AS min_price, 
       AVG(viewing_price) AS avg_price
FROM "videos";
```

2. **Получить количество уникальных пользователей, которые загрузили видео**
```sql
SELECT COUNT(DISTINCT user_id) AS unique_users
FROM "userdownloads";
```

3. **Получить количество загрузок для каждого видео**
```sql
SELECT video_id, COUNT(*) AS download_count
FROM "userdownloads"
GROUP BY video_id;
```

#### 4.2 HAVING

1. **Получить список видео, которые были загружены более чем 2 пользователями**
```sql
SELECT video_id, COUNT(*) AS user_count
FROM "userdownloads"
GROUP BY video_id
HAVING COUNT(*) > 2;
```

2. **Получить список пользователей, которые загрузили более чем 1 видео**
```sql
SELECT user_id, COUNT(*) AS download_count
FROM "userdownloads"
GROUP BY user_id
HAVING COUNT(*) > 1;
```

### Комментарии к запросам

#### Простые подзапросы

1. **Получить все видео, которые имеют цену просмотра выше средней цены просмотра всех видео**

   Этот запрос сначала вычисляет среднюю цену просмотра всех видео, а затем выбирает те видео, цена просмотра которых выше этой средней.

#### Коррелированные подзапросы

1. **Получить список пользователей и количество видео, которые они загрузили**

   В этом запросе для каждого пользователя в основной выборке (из таблицы `users`) выполняется подзапрос для подсчета количества загрузок видео этим пользователем.

#### Функции группировки

1. **Получить максимальную, минимальную и среднюю цену просмотра всех видео**

   Запрос использует агрегатные функции `MAX`, `MIN` и `AVG`, чтобы вычислить максимальную, минимальную и среднюю цену просмотра всех видео в таблице `videos`.

#### HAVING

1. **Получить список видео, которые были загружены более чем 2 пользователями**

   Этот запрос сначала группирует записи по `video_id`, затем использует `HAVING` для фильтрации групп, которые имеют более двух загрузок.


### 5. Функции и конструкции

#### 5.1 UPPER

1. **Получить имена всех пользователей в верхнем регистре**
```sql
SELECT UPPER(full_name) AS full_name_upper
FROM "users";
```

#### 5.2 LOWER

1. **Получить имена всех пользователей в нижнем регистре**
```sql
SELECT LOWER(full_name) AS full_name_lower
FROM "users";
```

#### 5.3 LIKE

1. **Найти всех пользователей, чьи имена начинаются с 'Иван'**
```sql
SELECT *
FROM "users"
WHERE full_name LIKE 'Иван%';
```

#### 5.4 CASE

1. **Показать видео и отметить, является ли их цена просмотра выше или ниже средней цены просмотра**
```sql
SELECT video_number, viewing_price,
       CASE 
           WHEN viewing_price > (SELECT AVG(viewing_price) FROM "videos") THEN 'Above Average'
           ELSE 'Below Average'
       END AS price_category
FROM "videos";
```

#### 5.5 BETWEEN

1. **Найти все видео, загруженные в период с '2023-02-01' по '2023-04-01'**
```sql
SELECT *
FROM "videos"
WHERE upload_date BETWEEN '2023-02-01' AND '2023-04-01';
```

#### 5.6 EXISTS

1. **Найти всех пользователей, которые загрузили хотя бы одно видео**
```sql
SELECT *
FROM "users" u
WHERE EXISTS (SELECT 1 FROM "userdownloads" ud WHERE ud.user_id = u.user_id);
```

#### 5.7 Вывод подстроки

1. **Получить первые три символа из имен пользователей**
```sql
SELECT SUBSTRING(full_name FROM 1 FOR 3) AS name_prefix
FROM "users";
```

### Комментарии к запросам

#### UPPER и LOWER

Эти запросы преобразуют регистр символов в столбце `full_name` у всех пользователей к верхнему и нижнему регистру соответственно.

#### LIKE

Запрос использует конструкцию `LIKE`, чтобы найти всех пользователей, чьи имена начинаются с 'Иван'. Символ `%` означает любую последовательность символов.

#### CASE

Этот запрос использует конструкцию `CASE`, чтобы классифицировать видео как имеющие цену просмотра выше или ниже средней.

#### BETWEEN

Запрос находит все видео, загруженные в указанный период. `BETWEEN` включает границы, то есть даты '2023-02-01' и '2023-04-01' также будут включены в результат.

#### EXISTS

Запрос проверяет существование хотя бы одной записи в таблице `userdownloads`, соответствующей каждому пользователю. Если такая запись существует, пользователь включается в результат.

#### Вывод подстроки

Запрос использует функцию `SUBSTRING` для извлечения первых трех символов из имени каждого пользователя.


### 6. Работа с NULL

#### 6.1 Обработка (сравнение)

1. **Найти всех пользователей, у которых номер телефона не указан (NULL)**
```sql
SELECT *
FROM "users"
WHERE phone IS NULL;
```

2. **Найти всех пользователей, у которых номер телефона указан (не NULL)**
```sql
SELECT *
FROM "users"
WHERE phone IS NOT NULL;
```

#### 6.2 Функции

1. **Использование функции COALESCE для замены NULL значений**
```sql
-- Предположим, что у нас есть столбец "middle_name", который может содержать NULL
SELECT user_id, full_name, COALESCE(phone, 'Не указано') AS phone_number
FROM "users";
```

2. **Использование функции NULLIF для сравнения и возврата NULL при совпадении значений**
```sql
-- Если цена просмотра равна 0, вернуть NULL
SELECT video_id, video_number, NULLIF(viewing_price, 0) AS adjusted_viewing_price
FROM "videos";
```

3. **Использование функции ISNULL (альтернативный синтаксис для COALESCE в некоторых системах)**
```sql
-- В PostgreSQL нет функции ISNULL, но она есть, например, в SQL Server. Пример для SQL Server:
SELECT user_id, full_name, ISNULL(phone, 'Не указано') AS phone_number
FROM "users";
```

### Комментарии к запросам

#### Обработка (сравнение) с NULL

1. **Найти всех пользователей, у которых номер телефона не указан (NULL)**

   Этот запрос выбирает всех пользователей, у которых значение столбца `phone` равно NULL. В SQL для проверки значений NULL используется конструкция `IS NULL`.

2. **Найти всех пользователей, у которых номер телефона указан (не NULL)**

   Этот запрос выбирает всех пользователей, у которых значение столбца `phone` не равно NULL. В SQL для проверки значений, которые не равны NULL, используется конструкция `IS NOT NULL`.

#### Функции

1. **COALESCE**

   Функция `COALESCE` принимает список аргументов и возвращает первый ненулевой аргумент. В этом запросе, если значение `phone` равно NULL, возвращается строка 'Не указано'.

2. **NULLIF**

   Функция `NULLIF` принимает два аргумента и возвращает NULL, если они равны, иначе возвращает первый аргумент. В этом запросе, если цена просмотра равна 0, возвращается NULL.

3. **ISNULL**

   Функция `ISNULL` работает аналогично `COALESCE` в SQL Server. Она принимает два аргумента и возвращает первый аргумент, если он не равен NULL, иначе возвращает второй аргумент.


### 7. Сортировка

#### 7.1 По возрастанию

1. **Сортировка видео по цене просмотра по возрастанию**
```sql
SELECT *
FROM "videos"
ORDER BY viewing_price ASC;
```

#### 7.2 По убыванию

1. **Сортировка видео по цене просмотра по убыванию**
```sql
SELECT *
FROM "videos"
ORDER BY viewing_price DESC;
```

#### 7.3 Множественная сортировка

1. **Сортировка видео сначала по типу видео по возрастанию, а затем по цене просмотра по убыванию**
```sql
SELECT *
FROM "videos"
ORDER BY video_type ASC, viewing_price DESC;
```

#### 7.4 С учётом значений NULL

1. **Сортировка пользователей по номеру телефона, при этом NULL значения будут в конце списка**
```sql
SELECT *
FROM "users"
ORDER BY phone IS NULL, phone ASC;
```

2. **Сортировка пользователей по номеру телефона, при этом NULL значения будут в начале списка**
```sql
SELECT *
FROM "users"
ORDER BY phone IS NOT NULL, phone ASC;
```

#### 7.5 По имени, псевдониму и номеру

1. **Сортировка видео по имени (названию), псевдониму (псевдоним столбца) и номеру**
```sql
-- Сортировка по названию (video_number)
SELECT *
FROM "videos"
ORDER BY video_number ASC;

-- Сортировка по псевдониму
SELECT video_number AS video_name
FROM "videos"
ORDER BY video_name ASC;

-- Сортировка по номеру столбца в SELECT (1 соответствует video_id)
SELECT video_id, video_number, video_type
FROM "videos"
ORDER BY 1 ASC;
```

### Комментарии к запросам

#### По возрастанию

1. **Сортировка видео по цене просмотра по возрастанию**

   Этот запрос сортирует все записи из таблицы `videos` по значению столбца `viewing_price` в порядке возрастания (`ASC`).

#### По убыванию

1. **Сортировка видео по цене просмотра по убыванию**

   Этот запрос сортирует все записи из таблицы `videos` по значению столбца `viewing_price` в порядке убывания (`DESC`).

#### Множественная сортировка

1. **Сортировка видео сначала по типу видео по возрастанию, а затем по цене просмотра по убыванию**

   Этот запрос сортирует записи сначала по значению столбца `video_type` в порядке возрастания, а затем по значению столбца `viewing_price` в порядке убывания.

#### С учётом значений NULL

1. **Сортировка пользователей по номеру телефона, при этом NULL значения будут в конце списка**

   Этот запрос сортирует записи таким образом, что записи с NULL значениями в столбце `phone` будут в конце списка. Конструкция `phone IS NULL` возвращает 1 для NULL значений и 0 для ненулевых, таким образом, NULL значения сортируются последними.

2. **Сортировка пользователей по номеру телефона, при этом NULL значения будут в начале списка**

   Этот запрос сортирует записи таким образом, что записи с NULL значениями в столбце `phone` будут в начале списка. Конструкция `phone IS NOT NULL` возвращает 1 для ненулевых значений и 0 для NULL, таким образом, NULL значения сортируются первыми.

#### По имени, псевдониму и номеру

1. **Сортировка по имени (названию), псевдониму (псевдоним столбца) и номеру**

   - Сортировка по названию (video_number): Этот запрос сортирует все записи из таблицы `videos` по значению столбца `video_number` в порядке возрастания.
   
   - Сортировка по псевдониму: Этот запрос сортирует записи по значению псевдонима `video_name`, который является псевдонимом для столбца `video_number`.
   
   - Сортировка по номеру столбца в SELECT: Этот запрос сортирует записи по значению первого столбца в списке SELECT, который соответствует `video_id`.


### 8. Объединение запросов

#### 8.1 UNION

1. **Объединение результатов двух запросов без дубликатов**
```sql
(SELECT video_id FROM "userdownloads")
UNION
(SELECT video_id FROM "videos");
```

#### 8.2 UNION ALL

1. **Объединение результатов двух запросов с учетом всех строк (включая дубликаты)**
```sql
(SELECT video_id FROM "userdownloads")
UNION ALL
(SELECT video_id FROM "videos");
```

#### 8.3 INTERSECT

1. **Получение пересечения результатов двух запросов**
```sql
(SELECT user_id FROM "userdownloads")
INTERSECT
(SELECT user_id FROM "users");
```

#### 8.4 EXCEPT

1. **Получение разницы между результатами двух запросов**
```sql
(SELECT user_id FROM "users")
EXCEPT
(SELECT user_id FROM "userdownloads");
```

### Комментарии к запросам

#### UNION

1. **Объединение результатов двух запросов без дубликатов**

   Этот запрос объединяет результаты двух запросов (выборка всех `video_id` из таблицы "userdownloads" и "videos") без учета повторяющихся значений.

#### UNION ALL

1. **Объединение результатов двух запросов с учетом всех строк (включая дубликаты)**

   Этот запрос объединяет результаты двух запросов (выборка всех `video_id` из таблицы "userdownloads" и "videos") с учетом всех строк, включая возможные дубликаты.

#### INTERSECT

1. **Получение пересечения результатов двух запросов**

   Этот запрос возвращает только те значения `user_id`, которые присутствуют в обоих запросах (выборка `user_id` из таблицы "userdownloads" и "users").

#### EXCEPT

1. **Получение разницы между результатами двух запросов**

   Этот запрос возвращает только те значения `user_id`, которые присутствуют в первом запросе (выборка `user_id` из таблицы "users"), но отсутствуют во втором запросе (выборка `user_id` из таблицы "userdownloads").


### 9. Таблицы

#### 9.1 Создание

1. **Создание таблицы "videos"**

```sql
CREATE TABLE IF NOT EXISTS "videos" (
    "video_id" serial PRIMARY KEY,
    "video_number" bigint NOT NULL,
    "video_type" text NOT NULL,
    "upload_date" date NOT NULL,
    "supplier_company" text NOT NULL,
    "total_duration" double precision NOT NULL,
    "category" text NOT NULL,
    "viewing_price" double precision NOT NULL
);
```

#### 9.2 Модификация

1. **Добавление нового столбца "description" в таблицу "videos"**

```sql
ALTER TABLE "videos"
ADD COLUMN "description" text;
```

#### 9.3 Ключи

1. **Создание составного первичного ключа для таблицы "orders"**

```sql
ALTER TABLE "orders"
ADD PRIMARY KEY ("user_id", "order_date");
```

#### 9.4 Индексы

1. **Создание индекса для ускорения поиска по столбцу "username" в таблице "users"**

```sql
CREATE INDEX idx_username ON "users"("username");
```

#### 9.5 UNIQUE

1. **Добавление ограничения UNIQUE на столбец "email" в таблице "users"**

```sql
ALTER TABLE "users"
ADD CONSTRAINT unique_email UNIQUE ("email");
```

#### 9.6 Ограничения

1. **Добавление внешнего ключа на столбец "category_id" в таблице "videos"**

```sql
ALTER TABLE "videos"
ADD CONSTRAINT fk_category_id
FOREIGN KEY ("category_id") REFERENCES "categories"("category_id");
```

#### 9.7 Значения по умолчанию

1. **Установка значения по умолчанию 'active' для столбца "status" в таблице "users"**

```sql
ALTER TABLE "users"
ALTER COLUMN "status" SET DEFAULT 'active';
```

#### 9.8 Последовательности

1. **Создание последовательности "user_id_seq"**

```sql
CREATE SEQUENCE "user_id_seq";
```

### Комментарии к запросам

- Создание таблицы, модификация структуры, создание ключей, индексов, уникальных ограничений, ограничений, значений по умолчанию и последовательностей являются стандартными операциями для работы с таблицами в СУБД. Каждый из приведенных запросов выполняет конкретную задачу, связанную с созданием или модификацией структуры таблицы, настройкой индексов и ключей для оптимизации запросов и обеспечения целостности данных.